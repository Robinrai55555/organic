from django.db import models

# Create your models here.


class header(models.Model):
    image = models.ImageField(upload_to="vegitable")
    name= models.CharField(max_length=200)
    def __str__(self):
        return self.name


class Categoty(models.Model):
    title = models.CharField(max_length=300)
    image = models.ImageField(upload_to="category")

    def __str__(self):
    	return self.title

class product(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to="product")
    price = models.DecimalField(max_digits=19, decimal_places=3)
    category = models.ForeignKey(Categoty, on_delete=models.CASCADE)

    def __str__(self):
    	return self.name