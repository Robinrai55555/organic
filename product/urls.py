from django.urls import path
from .views import*


app_name = "product"

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('product/', ProductListView.as_view(), name='product'),
    path('product/<int:pk>/details/', ProductDetailsView.as_view(), name='productdetail'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(),name='logout'),

]