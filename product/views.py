from django.contrib.auth import authenticate, login, logout
from django.views.generic import*
from django.shortcuts import render
from .forms import*
from .models import*



class Home(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['header'] = header.objects.all()
        context['categoryy'] = Categoty.objects.all()
        context['produtt'] = product.objects.all()

        return context


# Create your views here.
class ProductListView(ListView):
    template_name = "product.html"
    queryset = product.objects.all()
    context_objects_name = 'headerr'


class ProductDetailsView(DeleteView):
    template_name = "productdetal.html"
    model = product
    get_context_data = 'detailproduct'


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        r = form.cleaned_data['username']
        s = form.cleaned_data['password']
        usr = authenticate(username=r, password=s)
        if usr is not None:
            login(self.request, usr)
        else:
            return render(self.request, "login.html", {'error': "invalid username or password", "form": form})
        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class RegistrationView(FormView):
    template_name = "registration"
    form_class = RegistrationForm
    success_url = ''

    def form_valid(self, form):
        a = form.cleaned_data['username']
        b = form.cleaned_data['email']
        c = form.cleaned_data['password']
        User.objects.creat_user(a, b, c)
        return super().form_valid(form)
