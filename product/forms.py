from django import forms
from .models import*


class HeaderForms(forms.Form):
    class Meta:
        model = header
        fields = ['image', 'name']
        widgets = {
            'image': forms.ClearableFileInput(attrs={'class': 'form-control mb-2', 'placeholder': 'enter image'}),
            'name': forms.TextInput(attrs={'class': 'form-control mb-2', 'placeholder': 'enter name'}),
        }


class CategoryForm(forms.Form):
    class Meta:
        model = Categoty
        fields = ['title', 'image']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control mb-2', 'placeholder': 'enter class'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control mb-2', 'placeholder': 'enter image'
            }),
        }


class ProudctForm(forms.Form):
    class Meta:
        model = product
        fields = ['name', 'price', 'image', 'category']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control mb-2', 'placeholder': 'enter name'}),
            'price': forms.NumberInput(attrs={'class': 'form-control mb-2', 'placeholder': 'enter price'}),
            'image': forms.ClearableFileInput(attrs={'class': 'forms-control mb-2', 'placeholder': 'enter image'}),
            'category': forms.Select(attrs={'class': 'forms-control mb-2', 'placeholder': 'enter category'}),
        }


class RegistrationForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    email = forms.CharField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.TextInput())
    confirm_password = forms.CharField(widget=forms.TextInput())

    def cleaned_confirm_password(self):
        a = self.cleaned_data['password']
        b = self.cleaned_data['cleaned_confirm_password']
        if a != b:
            raise forms.Validationerror('your password didnot match')
        return b

    def clean_username(self):
        e = self.cleaned_data['username']
        if User.objects.filter(username=e).exists():
            raise forms.Validationerror('username is already exists')
        return e

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())

